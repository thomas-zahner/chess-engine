use std::convert::TryFrom;
use std::fmt::{Display, Formatter};

use crate::direction::Direction;

#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Position {
    pub file: i8,
    pub rank: i8,
}

impl Position {
    pub fn at(valid_position: &str) -> Self {
        Self::try_from(valid_position).unwrap()
    }

    pub fn new(file: i8, rank: i8) -> Self {
        Self { file, rank }
    }

    pub fn is_in_bounds(&self) -> bool {
        self.file >= 0 && self.rank >= 0 && self.file < 8 && self.rank < 8
    }

    pub fn is_dark_square_position(&self) -> bool {
        (self.file % 2 == 0 && self.rank % 2 == 0) || (self.file % 2 == 1 && self.rank % 2 == 1)
    }

    pub fn distance_from_center(&self) -> u8 {
        let file_distance = if self.file <= 3 { 3 - self.file } else { self.file - 4 };
        let rank_distance = if self.rank <= 3 { 3 - self.rank } else { self.rank - 4 };
        file_distance.max(rank_distance) as u8
    }

    pub fn change(&mut self, number: i8, direction: &Direction) {
        use Direction::*;

        let mut basic_operation = |direction: &Direction| {
            match direction {
                North => self.rank += number,
                East => self.file += number,
                South => self.rank -= number,
                West => self.file -= number,
                _ => unreachable!()
            }
        };

        match direction {
            NorthEast => {
                basic_operation(&North);
                basic_operation(&East);
            }
            SouthEast => {
                basic_operation(&South);
                basic_operation(&East);
            }
            SouthWest => {
                basic_operation(&South);
                basic_operation(&West);
            }
            NorthWest => {
                basic_operation(&North);
                basic_operation(&West);
            }
            _ => basic_operation(direction)
        }
    }
}

impl TryFrom<String> for Position {
    type Error = &'static str;

    fn try_from(position: String) -> Result<Self, Self::Error> {
        let mut chars = position.chars();
        let file = chars.next();
        let rank = chars.next();

        if file.is_none() || rank.is_none() || chars.next().is_some() {
            return Err("Unexpected size");
        }

        let rank = rank.unwrap().to_digit(10);
        let file = file.unwrap().to_ascii_lowercase();

        let rank = match rank {
            None => return Err("Invalid rank"),
            Some(n) => {
                match n {
                    1..=8 => n as i8 - 1,
                    _ => return Err("Unknown rank")
                }
            }
        };

        let file = match file {
            'a'..='h' => file as i8 - 'a' as i8,
            _ => return Err("Unknown file")
        };

        Ok(Position::new(file, rank))
    }
}

impl TryFrom<&str> for Position {
    type Error = &'static str;
    fn try_from(position: &str) -> Result<Self, Self::Error> {
        Position::try_from(position.to_string())
    }
}

impl From<Position> for String {
    fn from(position: Position) -> Self {
        if !position.is_in_bounds() {
            format!("Out of bounds: file = {}, rank = {}", position.file, position.rank)
        } else {
            let file = (b'a' + position.file as u8) as char;
            let rank = position.rank + 1;
            format!("{}{}", file, rank)
        }
    }
}

impl Display for Position {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        let string: String = (*self).into();
        write!(f, "{}", string)
    }
}

impl std::fmt::Debug for Position {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        self::Display::fmt(self, f)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn from_into() {
        let input = "a1";
        let target = Position::try_from(input);
        assert!(target.is_ok());

        let output: String = target.unwrap().into();
        assert_eq!(input, output);
    }

    #[test]
    fn from_min_valid_string() {
        let p = Position::try_from("a1");
        assert!(p.is_ok());
        assert_eq!(p.unwrap().rank, 0);
        assert_eq!(p.unwrap().file, 0);
    }

    #[test]
    fn from_max_valid_string() {
        let p = Position::try_from("h8");
        assert!(p.is_ok());
        assert_eq!(p.unwrap().rank, 7);
        assert_eq!(p.unwrap().file, 7);
    }

    #[test]
    fn from_invalid_rank() {
        assert!(Position::try_from("c9").is_err());
    }

    #[test]
    fn from_invalid_file() {
        assert!(Position::try_from("i6").is_err());
    }

    #[test]
    fn from_string_too_big() {
        let p = Position::try_from("a10");
        assert!(p.is_err());
        assert_eq!(p.err().unwrap(), "Unexpected size");
    }

    #[test]
    fn from_string_too_small() {
        let p = Position::try_from("a");
        assert!(p.is_err());
        assert_eq!(p.err().unwrap(), "Unexpected size");
    }

    #[test]
    fn from_uppercase_file() {
        let p = Position::try_from("E4");
        assert!(p.is_ok());

        let string: String = Position::from(p.unwrap()).into();
        assert_eq!(string, "e4");
    }

    #[test]
    fn distance_from_center() {
        assert_eq!(Position::at("d4").distance_from_center(), 0);
        assert_eq!(Position::at("e5").distance_from_center(), 0);

        assert_eq!(Position::at("a1").distance_from_center(), 3);
        assert_eq!(Position::at("c2").distance_from_center(), 2);
    }
}
