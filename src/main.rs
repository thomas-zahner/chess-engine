use crate::board::Board;
use crate::game::Game;
use crate::player::Player;

mod algorithm;
mod analysis;
mod board;
mod castle;
mod command;
mod direction;
mod game;
mod piece;
mod play_game;
mod player;
mod position;
mod puzzles;
mod score;
mod square;

fn main() {
    let game = Game::new(Board::new(), Player::White);
    play_game::start_game(game);
}
