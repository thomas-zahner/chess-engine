use crate::player::Player;
use crate::piece::Piece;
use crate::position::Position;

#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Square {
    piece: Option<Piece>,
    owner: Option<Player>,
    position: Position,
}

impl Square {
    pub fn new(position: Position) -> Self {
        Self { piece: None, owner: None, position }
    }

    pub fn set(&mut self, piece: Piece, owner: Player) {
        self.piece = Some(piece);
        self.owner = Some(owner);
    }

    pub fn unset(&mut self) {
        self.piece = None;
        self.owner = None;
    }

    pub fn is_empty(&self) -> bool { self.owner.is_none() }

    pub fn occupied_by(&self, player: Player, piece: Piece) -> bool {
        if self.is_empty() { return false; }
        self.get_piece() == piece && self.get_owner() == player
    }

    pub fn get_owner(&self) -> Player {
        if self.is_empty() { panic!("Square is empty") }
        self.owner.unwrap()
    }

    pub fn get_piece(&self) -> Piece {
        if self.is_empty() { panic!("Square is empty") }
        self.piece.unwrap()
    }

    pub fn get_position(&self) -> Position { self.position }

    pub fn string_representation(&self) -> &str {
        if self.is_empty() {
            return if self.position.is_dark_square_position() { "#" } else { " " };
        }

        let owner = &self.owner.unwrap();
        let piece = &self.piece.unwrap();

        use Piece::*;

        match owner {
            Player::White => {
                match piece {
                    Pawn => "♙",
                    Knight => "♘",
                    Bishop => "♗",
                    Rook => "♖",
                    Queen => "♕",
                    King => "♔",
                }
            }
            Player::Black => {
                match piece {
                    Pawn => "♟",
                    Knight => "♞",
                    Bishop => "♝",
                    Rook => "♜",
                    Queen => "♛",
                    King => "♚",
                }
            }
        }
    }
}
