#[derive(Eq, PartialEq, Copy, Clone)]
pub enum Direction {
    North,
    East,
    South,
    West,

    NorthEast,
    SouthEast,
    SouthWest,
    NorthWest,
}
