use std::cmp::Ordering;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use std::time::Instant;

use threadpool::ThreadPool;

use crate::analysis::all_possibilities;
use crate::board::Board;
use crate::castle;
use crate::piece::Piece;
use crate::player::Player;
use crate::position::Position;
use crate::score::Score;

type Possibility = (Position, Position, Score);

pub fn best_moves(board: &Board, player: Player, depth: u8) -> Vec<(Position, Position)> {
    let history = Arc::new(Mutex::new(HashMap::new()));
    let mut states = vec![];

    board.get_squares(player).iter().for_each(|from| {
        for (board, to) in all_possibilities(from, board) {
            states.push((board, from.get_position(), to));
        }
    });

    let results = Arc::new(Mutex::new(vec![]));
    let pool = ThreadPool::new(num_cpus::get());

    for (board, from, to) in states {
        let history = Arc::clone(&history);
        let results = Arc::clone(&results);

        pool.execute(move || {
            let begin = Instant::now();
            let score = recurse(&board, player.opponent(), player, 1, depth, &history, Pruning::new());
            println!("{} -> {} = {}, {:?}", from, to, score, Instant::now() - begin);
            results.lock().unwrap().push((from, to, score));
        });
    }

    pool.join();

    let results = results.lock().unwrap().to_vec();
    use_preferred_moves(results, board)
}

/// Pick the preferred moves by using a few simple rules
fn use_preferred_moves(possibilities: Vec<Possibility>, board: &Board) -> Vec<(Position, Position)> {
    let moves: Vec<(Position, Position)> =
        find_all(true, possibilities, |(_, _, a), (_, _, b)| a.cmp(b))
            .into_iter().map(|(from, to, _)| (from, to)).collect();

    let castling_moves: Vec<(Position, Position)> =
        moves.clone().into_iter().filter(|(from, to)| {
            castle::handle_castling_move(&mut board.clone(),
                                         board.at(*from), *to)
        }).collect();

    if !castling_moves.is_empty() { return castling_moves; }

    find_all(false, moves, |(source_a, target_a), (source_b, target_b)| {
        let distance = |from: Position, to: Position| {
            let mut board = board.clone();
            let square = board.at(from);
            let player = square.get_owner();

            match square.get_piece() {
                // Don't try to get the king or queen closer to the center
                Piece::King | Piece::Queen => u8::MAX,
                _ => {
                    board.move_piece(from, to);
                    board.summed_distance_from_center(player)
                }
            }
        };

        distance(*source_a, *target_a).cmp(&distance(*source_b, *target_b))
    })
}

fn find_all<F, T>(maximum: bool, items: Vec<T>, f: F) -> Vec<T>
    where T: Copy, F: for<'a, 'b> Fn(&'a &T, &'b &T) -> Ordering {
    let mut results = vec![];

    let min_or_max = if maximum { Iterator::max_by } else { Iterator::min_by };
    let matching = *match min_or_max(items.iter(), &f) {
        None => return vec![],
        Some(value) => value
    };

    for current in items {
        if f(&&current, &&matching) == Ordering::Equal {
            results.push(current);
        }
    }

    results
}

fn recurse(board: &Board, turn: Player, player: Player, depth: u8, max_depth: u8,
           history: &Arc<Mutex<HashMap<(Board, u8), Score>>>, mut pruning: Pruning) -> Score {
    let score = board.relative_piece_values(player);

    if depth == max_depth {
        return score;
    } else {
        match score {
            Score::Lost | Score::Won => return score,
            Score::Some(_) => {}
        }
    }

    let maximising = turn == player;
    let mut best_score = if maximising { Score::Lost } else { Score::Won };

    for board in get_all_new_board_states(board, turn) {
        let use_history = depth < 4;

        if use_history {
            if let Some(stored_score) = history.lock().unwrap().get(&(board.clone(), depth)) {
                match update_best_score(*stored_score, &mut best_score, &mut pruning, maximising) {
                    None => {}
                    Some(score) => return score
                }
                continue;
            }
        }

        let score = recurse(&board, turn.opponent(), player, depth + 1, max_depth, history, pruning);

        match update_best_score(score, &mut best_score, &mut pruning, maximising) {
            None => {}
            Some(score) => return score
        }

        if use_history {
            history.lock().unwrap().insert((board.clone(), depth), score);
        }

        if pruning.can_prune() { break; }
    }

    best_score
}

fn get_all_new_board_states(board: &Board, turn: Player) -> Vec<Board> {
    let mut boards = vec![];

    for square in board.get_squares(turn) {
        for (board, _) in all_possibilities(&square, board) {
            boards.push(board);
        }
    }
    boards
}

/// Returns `Some(Score)` if we have lost or won and don't need to search any further
fn update_best_score(score: Score, best_score: &mut Score, pruning: &mut Pruning, maximising: bool) -> Option<Score> {
    if maximising {
        if score == Score::Won { return Some(score); }

        if &score > best_score {
            *best_score = score;
            pruning.maximise_alpha(*best_score);
        }
    } else {
        if score == Score::Lost { return Some(score); }

        if &score < best_score {
            *best_score = score;
            pruning.minimise_beta(*best_score);
        }
    }

    None
}

#[derive(Copy, Clone)]
struct Pruning {
    alpha: Score,
    beta: Score,
}

impl Pruning {
    fn new() -> Self { Self { alpha: Score::Lost, beta: Score::Won } }

    fn can_prune(&self) -> bool {
        self.alpha >= self.beta
    }

    fn maximise_alpha(&mut self, alpha: Score) {
        self.alpha = self.alpha.max(alpha);
    }

    fn minimise_beta(&mut self, beta: Score) {
        self.beta = self.beta.max(beta);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn max_all() {
        let x = find_all(true, vec![1, 7, 2, 7], |a, b| a.cmp(b));
        assert_eq!(vec![7, 7], x);
    }

    #[test]
    fn min_all() {
        let x = find_all(false, vec![0, 0, 7], |a, b| a.cmp(b));
        assert_eq!(vec![0, 0], x);
    }
}

