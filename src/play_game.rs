use std::io;

use crate::command;
use crate::game::Game;

pub fn start_game(mut game: Game) {
    loop { parse_command(&mut game) }
}

fn parse_command(game: &mut Game) {
    let input = read_from_stdin();
    let mut arguments: Vec<&str> = input.split(' ').collect();

    let command = arguments.remove(0);
    if command.is_empty() { return; }

    for c in command::get_commands() {
        if c.command() == command {
            if command::valid_argument_count(&arguments.len(), &*c) {
                c.run(&arguments, game);
            } else {
                println!("Invalid argument count");
            }

            return;
        }
    }

    println!("Unknown command. Type 'help' to see the list of commands")
}

fn read_from_stdin() -> String {
    let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap();
    input.pop(); //Pop the newline
    input
}

