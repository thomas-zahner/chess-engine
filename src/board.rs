use std::fmt::{Display, Formatter, Result};

use crate::piece::Piece;
use crate::player::Player;
use crate::position::Position;
use crate::score::Score;
use crate::square::Square;

#[derive(Clone, Eq, PartialEq, Hash)]
pub struct Board {
    board: [[Square; 8]; 8],
}

impl Board {
    pub fn new() -> Self {
        let mut board = Self::empty_board();

        board[0][0].set(Piece::Rook, Player::White);
        board[0][7].set(Piece::Rook, Player::White);

        board[0][1].set(Piece::Knight, Player::White);
        board[0][6].set(Piece::Knight, Player::White);

        board[0][2].set(Piece::Bishop, Player::White);
        board[0][5].set(Piece::Bishop, Player::White);

        board[0][3].set(Piece::Queen, Player::White);
        board[0][4].set(Piece::King, Player::White);

        (0..8).for_each(|file| {
            board[7][file].set(board[0][file].get_piece(), Player::Black);
        });

        (0..8).for_each(|file| {
            board[1][file].set(Piece::Pawn, Player::White);
            board[6][file].set(Piece::Pawn, Player::Black);
        });

        Self { board }
    }

    pub fn new_empty() -> Self {
        Self { board: Self::empty_board() }
    }

    fn empty_board() -> [[Square; 8]; 8] {
        let mut board: [[Square; 8]; 8] = [[Square::new(Position::new(0, 0)); 8]; 8];

        for (y, rank) in board.iter_mut().enumerate() {
            for (x, square) in rank.iter_mut().enumerate() {
                *square = Square::new(Position { rank: y as i8, file: x as i8 });
            }
        }

        board
    }

    pub fn at(&self, position: Position) -> &Square {
        if !position.is_in_bounds() { panic!("Invalid position"); }
        &self.board[position.rank as usize][position.file as usize]
    }

    pub fn at_mutable(&mut self, position: Position) -> &mut Square {
        if !position.is_in_bounds() { panic!("Invalid position"); }
        &mut self.board[position.rank as usize][position.file as usize]
    }

    pub fn move_piece(&mut self, from: Position, to: Position) {
        let source = *self.at(from);

        if crate::castle::handle_castling_move(self, &source, to) { return; }
        if self.handle_pawn_last_rank(&source, to) { return; }

        self.at_mutable(to).set(source.get_piece(), source.get_owner());
        self.at_mutable(from).unset();
    }

    pub fn get_squares(&self, player: Player) -> Vec<Square> {
        let mut squares = vec![];

        self.for_each(|square| {
            if !square.is_empty() && square.get_owner() == player {
                squares.push(*square);
            }
        });

        squares
    }

    pub fn relative_piece_values(&self, player: Player) -> Score {
        let mut value = 0;

        let mut our_king_exists = false;
        let mut opponent_king_exists = false;

        self.for_each(|square| {
            if !square.is_empty() {
                let piece_value = square.get_piece().relative_value();

                if square.get_owner() == player {
                    if square.get_piece() == Piece::King {
                        our_king_exists = true;
                    }

                    value += piece_value
                } else {
                    if square.get_piece() == Piece::King {
                        opponent_king_exists = true;
                    }

                    value -= piece_value
                }
            }
        });

        if !our_king_exists {
            Score::Lost
        } else if !opponent_king_exists {
            Score::Won
        } else {
            Score::Some(value)
        }
    }

    pub fn summed_distance_from_center(&self, player: Player) -> u8 {
        let mut distance = 0;
        for square in self.get_squares(player) {
            distance += square.get_position().distance_from_center();
        }
        distance
    }

    fn handle_pawn_last_rank(&mut self, source: &Square, to: Position) -> bool {
        if source.get_piece() != Piece::Pawn { return false; }

        let player = source.get_owner();
        let last_rank = match player {
            Player::White => 7,
            Player::Black => 0,
        };

        if to.rank == last_rank {
            self.at_mutable(source.get_position()).unset();
            self.at_mutable(to).set(Piece::Queen, player);
            return true;
        }

        false
    }

    fn for_each<F>(&self, mut action: F) where F: FnMut(&Square) {
        for y in (0..8).rev() {
            for x in 0..8 {
                action(&self.board[y][x]);
            }
        }
    }
}

impl Display for Board {
    fn fmt(&self, f: &mut Formatter) -> Result {
        let mut string: String = String::with_capacity(200);

        self.for_each(|square| {
            string += square.string_representation();
            string += " ";
            if square.get_position().file == 7 { string += "\n"; }
        });

        write!(f, "{}", string)
    }
}