#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub enum Player {
    Black,
    White,
}

impl Player {
    pub fn opponent(&self) -> Player {
        use Player::*;
        match self {
            Black => White,
            White => Black,
        }
    }
}

