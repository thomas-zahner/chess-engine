#[cfg(test)]
mod puzzles {
    use crate::algorithm::best_moves;
    use crate::board::Board;
    use crate::piece::Piece;
    use crate::piece::Piece::*;
    use crate::player::Player;
    use crate::player::Player::*;
    use crate::position::Position;

    #[test]
    fn puzzle() {
        let board = &mut Board::new_empty();

        add(White, King, "d6", board);
        add(White, Queen, "d2", board);
        add(White, Pawn, "a3", board);
        add(White, Pawn, "h2", board);
        add(White, Pawn, "d5", board);
        add(White, Pawn, "f5", board);
        add(White, Pawn, "g6", board);

        add(Black, King, "d8", board);
        add(Black, Queen, "b1", board);
        add(Black, Rook, "e5", board);
        add(Black, Pawn, "a4", board);
        add(Black, Pawn, "f6", board);
        add(Black, Pawn, "g7", board);
        add(Black, Pawn, "h6", board);

        println!("{}", board);

        let moves = best_moves(board, White, 4);
        assert_eq!(moves.len(), 1);

        let (from, to) = moves[0];
        assert_eq!(from, Position::at("d2"));
        assert_eq!(to, Position::at("a5"));
    }

    fn add(owner: Player, piece: Piece, position: &str, board: &mut Board) {
        board.at_mutable(Position::at(position)).set(piece, owner);
    }
}
