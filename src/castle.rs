use CastleType::*;

use crate::board::Board;
use crate::direction::Direction::*;
use crate::piece::Piece::*;
use crate::player::Player;
use crate::position::Position;
use crate::square::Square;

type State = (Board, Position);

enum CastleType {
    QueenSide,
    KingSide,
}

pub fn handle_castling_move(board: &mut Board, source: &Square, to: Position) -> bool {
    if source.get_piece() == King {
        let possibilities = possibilities_castling(source, board);

        for (new_board, target) in possibilities {
            if target == to {
                *board = new_board;
                return true;
            }
        }
    }

    false
}

pub fn possibilities_castling(king_square: &Square, board: &Board) -> Vec<State> {
    let mut states = vec![];
    let player = king_square.get_owner();

    for castle_type in &[QueenSide, KingSide] {
        if let Some(state) = try_castle(castle_type, player, board) {
            states.push(state);
        }
    }

    states
}

fn try_castle(castle_type: &CastleType, player: Player, board: &Board) -> Option<State> {
    let rank = match player {
        Player::Black => 7,
        Player::White => 0,
    };

    let king_square = board.at(Position::new(4, rank));
    let rook_square = match &castle_type {
        QueenSide => board.at(Position::new(0, rank)),
        KingSide => board.at(Position::new(7, rank)),
    };

    if !king_square.occupied_by(player, King) || !rook_square.occupied_by(player, Rook) {
        return None;
    }

    let between_rook_and_king = match &castle_type {
        QueenSide => 1..4,
        KingSide => 5..7,
    };

    for file in between_rook_and_king {
        if !board.at(Position::new(file, rank)).is_empty() { return None; }
    }

    let (king_direction, rook_direction, rook_change) = match castle_type {
        QueenSide => (&West, &East, 3),
        KingSide => (&East, &West, 2),
    };

    let mut new_king_position = king_square.get_position();
    new_king_position.change(2, king_direction);

    let mut new_rook_position = rook_square.get_position();
    new_rook_position.change(rook_change, rook_direction);

    let mut board = board.clone();
    board.at_mutable(king_square.get_position()).unset();
    board.at_mutable(rook_square.get_position()).unset();

    board.at_mutable(new_king_position).set(King, player);
    board.at_mutable(new_rook_position).set(Rook, player);

    Some((board, new_king_position))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::player::Player::*;

    #[test]
    fn queen_side() {
        let board = simple_board();
        let result = try_castle(&QueenSide, White, &board);
        assert!(result.is_some());

        let (board, position) = result.unwrap();
        assert_eq!(position, Position::at("C1"));

        assert!(board.at(Position::at("C1")).occupied_by(White, King));
        assert!(board.at(Position::at("D1")).occupied_by(White, Rook));
    }

    #[test]
    fn king_side() {
        let board = simple_board();
        let result = try_castle(&KingSide, White, &board);
        assert!(result.is_some());

        let (board, position) = result.unwrap();
        assert_eq!(position, Position::at("G1"));

        assert!(board.at(Position::at("G1")).occupied_by(White, King));
        assert!(board.at(Position::at("F1")).occupied_by(White, Rook));
    }

    #[test]
    fn queen_side_piece_between() {
        let mut board = simple_board();
        board.at_mutable(Position::at("C1")).set(Bishop, White);
        assert!(try_castle(&QueenSide, White, &board).is_none());
    }

    #[test]
    fn king_side_piece_between() {
        let mut board = simple_board();
        board.at_mutable(Position::at("F1")).set(Bishop, White);
        assert!(try_castle(&KingSide, White, &board).is_none());
    }

    fn simple_board() -> Board {
        let mut board = Board::new_empty();
        board.at_mutable(Position::at("A1")).set(Rook, White);
        board.at_mutable(Position::at("E1")).set(King, White);
        board.at_mutable(Position::at("H1")).set(Rook, White);
        board
    }
}
