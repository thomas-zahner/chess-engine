use std::fmt::{Display, Formatter};

#[derive(Eq, PartialEq, Ord, PartialOrd, Copy, Clone)]
pub enum Score {
    Lost,
    Some(i8),
    Won,
}

impl Display for Score {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        let string = match self {
            Score::Lost => { "Lost".into() }
            Score::Won => { "Won".into() }
            Score::Some(score) => { format!("Some({})", score) }
        };
        write!(f, "{}", string)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn score_ord() {
        use Score::*;
        assert!(Lost < Won);
        assert!(Lost < Some(1));
        assert!(Some(3) > Some(1));
    }
}
